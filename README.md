# Assignment 2 - Trivia Game using Vue.js

We have build an online Trivia Game as a Single Page Application using Vue.js Framework.

![start page](https://gitlab.com/zamFe/assignment_5_vue/-/raw/main/src/assets/startpage.png)

### How to run:
- npm
- Node.js
- Heroku CLI (For Pushing Project To Heroku For Hosting)
- Vue.js

### Extra Features
- Scoreboard
- Session Token (So That You Don't Get the Same Questions)

## Project setup
```
npm install

Dev:
npm run serve

Build:
npm run build
```

### Hosted Solution
https://aqueous-island-98671.herokuapp.com/#/

### Component Tree
https://www.figma.com/file/xxhTIsVYWThV3CA5UjLQ7S/Assignment-5-Noroff?node-id=0%3A1

### Score API Server
https://noroff-assignment-5-api.herokuapp.com/

### Question API Server
https://opentdb.com/api_config.php
