export async function getQuestionCategories() {
    try {
        const {trivia_categories} = await fetch("https://opentdb.com/api_category.php")
            .then(response => response.json())
        return [null, trivia_categories]
    } catch (e) {
        return [e.message, {}]
    }
}